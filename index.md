---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
# Howdy
I'm Katie. My server's package manager replaced my old `index.html` and I didn't have any backups, so I figured I'd try out Jekyll. I'm too lazy to actually add any content, so this is it, I suppose.
